import { BlockGame } from './BlockGame';

const blockGame1: BlockGame = new BlockGame('BLOCK GAME 1', 'FINISH BLOCK GAME 1',  10, 10, 'O');
const blockGame2: BlockGame = new BlockGame('BLOCK GAME 2', 'FINISH BLOCK GAME 2',  20, 20, 'A');
const blockGame3: BlockGame = new BlockGame('BLOCK GAME 3', 'FINISH BLOCK GAME 3',  20, 10, 'X');

blockGame1.start();
blockGame2.start();
blockGame3.start();