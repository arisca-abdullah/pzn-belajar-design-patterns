import { BlockTemplate } from './BlockTemplate';

export class BlockGame extends BlockTemplate {
    title: string;
    endTitle: string;
    width: number;
    height: number;
    char: string;

    constructor(title: string, endTitle: string, width: number, height: number, char: string) {
        super();
        this.title = title;
        this.endTitle = endTitle;
        this.width = width;
        this.height = height;
        this.char = char;
    }
    
    getTitle() {
        return this.title;
    }

    getEndTitle() {
        return this.endTitle;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    getChar() {
        return this.char;
    }
}