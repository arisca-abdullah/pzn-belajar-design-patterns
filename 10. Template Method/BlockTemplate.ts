import { stdout } from 'process';

export abstract class BlockTemplate {
    start() {
        console.log(this.getTitle());

        for (let i = 0; i < this.getHeight(); i ++) {
            for (let i = 0; i < this.getWidth(); i++) {
                stdout.write(this.getChar());
            }
            console.log();
        }

        console.log(this.getEndTitle())
    }

    abstract getTitle(): string;
    abstract getEndTitle(): string;
    abstract getWidth(): number;
    abstract getHeight(): number;
    abstract getChar(): string;
}