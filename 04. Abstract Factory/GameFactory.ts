import { Arena, ArenaEasy, ArenaMedium, ArenaHard } from './Arena';
import { Level, LevelEasy, LevelMedium, LevelHard} from './Level';
import { Enemy, EnemyEasy, EnemyMedium, EnemyHard } from './Enemy';

export interface GameFactory {
    createArena(): Arena;
    createLevel(): Level;
    createEnemy(): Enemy;
}

export class GameFactoryEasy implements GameFactory {
    createArena() {
        return new ArenaEasy();
    }

    createLevel() {
        return new LevelEasy();
    }

    createEnemy() {
        return new EnemyEasy();
    }
}

export class GameFactoryMedium implements GameFactory {
    createArena() {
        return new ArenaMedium();
    }

    createLevel() {
        return new LevelMedium();
    }

    createEnemy() {
        return new EnemyMedium();
    }
}

export class GameFactoryHard implements GameFactory {
    createArena() {
        return new ArenaHard();
    }

    createLevel() {
        return new LevelHard();
    }

    createEnemy() {
        return new EnemyHard();
    }
}