export interface Arena {
    start(): void;
}

export class ArenaEasy implements Arena {
    start() {
        console.log('Arena Easy!');
    }
}

export class ArenaMedium implements Arena {
    start() {
        console.log('Arena Medium!');
    }
}

export class ArenaHard implements Arena {
    start() {
        console.log('Arena Hard!');
    }
}
