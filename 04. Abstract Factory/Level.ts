export interface Level {
    start(): void;
}

export class LevelEasy implements Level {
    start() {
        console.log('Level Easy!');
    }
}

export class LevelMedium implements Level {
    start() {
        console.log('Level Medium!');
    }
}

export class LevelHard implements Level {
    start() {
        console.log('Level Hard!');
    }
}
