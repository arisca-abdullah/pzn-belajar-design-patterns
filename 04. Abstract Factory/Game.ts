import { Arena } from './Arena';
import { Level } from './Level';
import { Enemy } from './Enemy';
import { GameFactory } from './GameFactory';

export class Game {
    private arena: Arena;
    private level: Level;
    private enemy: Enemy;

    constructor(gameFactory: GameFactory) {
        this.arena = gameFactory.createArena();
        this.level = gameFactory.createLevel();
        this.enemy = gameFactory.createEnemy();
    }

    start() {
        this.arena.start();
        this.level.start();
        this.enemy.start();
    }
}