import { Binatang } from './Binatang';

export abstract class BinatangDarat implements Binatang {
    abstract getNama(): string;
    
    hidupDiAir() {
        return false;
    }

    hidupDiDarat() {
        return true;
    }

    abstract getJumlahKaki(): number;
}