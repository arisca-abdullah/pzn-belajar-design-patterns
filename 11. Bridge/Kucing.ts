import { BinatangDarat } from './BinatangDarat';

export class Kucing extends BinatangDarat {
    getNama() {
        return 'Kucing';
    }

    getJumlahKaki() {
        return 4;
    }
}