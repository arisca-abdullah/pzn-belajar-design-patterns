export interface Binatang {
    getNama(): string;
    hidupDiAir(): boolean;
    hidupDiDarat(): boolean;
}