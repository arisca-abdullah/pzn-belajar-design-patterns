import { BinatangDarat } from './BinatangDarat';

export class Ayam extends BinatangDarat {
    getNama() {
        return 'Ayam';
    }

    getJumlahKaki() {
        return 2;
    }
}