import { Binatang } from './Binatang';
import { BinatangAir } from './BinatangAir';
import { BinatangDarat } from './BinatangDarat';
import { Anjing } from './Anjing';
import { Ayam } from './Ayam';
import { Hiu } from './Hiu';
import { Kambing } from './Kambing';
import { Koi } from './Koi';
import { Kucing } from './Kucing';
import { Lele } from './Lele';

const binatangs: Binatang[] = [
    new Anjing(),
    new Ayam(),
    new Hiu(),
    new Kambing(),
    new Koi(),
    new Kucing(),
    new Lele()
];

binatangs.forEach(binatang => {
    if (binatang instanceof BinatangAir) {
        console.log(binatang.getNama() + ' hidup di air');
    } else if (binatang instanceof BinatangDarat) {
        console.log(binatang.getNama() + ' hidup di darat dan memiliki ' + binatang.getJumlahKaki() + ' kaki')
    }
});