import { Binatang } from './Binatang';

export abstract class BinatangAir implements Binatang {
    abstract getNama(): string;
    
    hidupDiAir() {
        return true;
    }

    hidupDiDarat() {
        return false;
    }
}