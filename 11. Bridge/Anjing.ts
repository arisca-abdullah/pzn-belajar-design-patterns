import { BinatangDarat } from './BinatangDarat';

export class Anjing extends BinatangDarat {
    getNama() {
        return 'Anjing';
    }

    getJumlahKaki() {
        return 4;
    }
}