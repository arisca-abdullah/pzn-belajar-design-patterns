import { BinatangDarat } from './BinatangDarat';

export class Kambing extends BinatangDarat {
    getNama() {
        return 'Kambing';
    }

    getJumlahKaki() {
        return 4;
    }
}