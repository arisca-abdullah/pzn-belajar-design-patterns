import { Book } from './Book';
import { Screencast } from './Screencast';
import { CatalogAdapter } from './CatalogAdapter';
import { BookCatalogAdapter } from './BookCatalogAdapter';
import { ScreencastCatalogAdapter } from './ScreencastCatalogAdapter';

let list: CatalogAdapter[] = [];

list.push(new BookCatalogAdapter(new Book('Pemrograman Java', 'Eko')));
list.push(new BookCatalogAdapter(new Book('Pemrograman PHP', 'Kurniawan')));
list.push(new BookCatalogAdapter(new Book('Pemrograman Python', 'Khannedy')));

list.push(new ScreencastCatalogAdapter(new Screencast('Web Laravel', 'Joko', 100)));
list.push(new ScreencastCatalogAdapter(new Screencast('Web Rails', 'Rudi', 200)));
list.push(new ScreencastCatalogAdapter(new Screencast('Web Flask', 'Ardi', 150)));

list.forEach(item => {
    console.log(item.getCatalogTitle());
});