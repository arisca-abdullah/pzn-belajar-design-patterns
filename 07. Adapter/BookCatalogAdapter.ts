import { Book } from './Book';
import { CatalogAdapter } from './CatalogAdapter';

export class BookCatalogAdapter implements CatalogAdapter {
    private book: Book;

    constructor(book: Book) {
        this.book = book;
    }
    
    getCatalogTitle() {
        return this.book.getTitle() + ' by ' + this.book.getAuthor();
    }
}