export class Screencast {
    private title: string;
    private author: string;
    private duration: number;

    constructor(title: string, author: string, duration: number) {
        this.title = title;
        this.author = author;
        this.duration = duration;
    }

    setTitle(title: string) {
        this.title = title;
        return this;
    }

    getTitle() {
        return this.title;
    }

    setAuthor(author: string) {
        this.author = author;
        return this;
    }

    getAuthor() {
        return this.author;
    }

    setDuration(duration: number) {
        this.duration = duration;
        return this;
    }

    getDuration() {
        return this.duration;
    }
}