export class Book {
    private title: string;
    private author: string;

    constructor(title: string, author: string) {
        this.title = title;
        this.author = author;
    }

    setTitle(title: string) {
        this.title = title;
        return this;
    }

    getTitle() {
        return this.title;
    }

    setAuthor(author: string) {
        this.author = author;
        return this;
    }

    getAuthor() {
        return this.author;
    }
}