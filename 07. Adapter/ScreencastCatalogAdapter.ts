import { Screencast } from './Screencast';
import { CatalogAdapter } from './CatalogAdapter';

export class ScreencastCatalogAdapter implements CatalogAdapter {
    private screencast: Screencast;

    constructor(screencast: Screencast) {
        this.screencast = screencast;
    }
    
    getCatalogTitle() {
        return this.screencast.getTitle() + ' by ' + this.screencast.getAuthor();
    }
}