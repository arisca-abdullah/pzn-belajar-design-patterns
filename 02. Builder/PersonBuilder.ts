import { Person } from './Person';

export class PersonBuilder {
    private firstName: string = "";
    private lastName: string = "";
    private email: string = "";
    private phone: string = "";
    private address: string = "";
    private hobby: string = "";

    setFirstName(firstName: string) {
        this.firstName = firstName;
        return this;
    }

    setLastName(lastName: string) {
        this.lastName = lastName;
        return this;
    }

    setEmail(email: string) {
        this.email = email;
        return this;
    }

    setPhone(phone: string) {
        this.phone = phone;
        return this;
    }

    setAddress(address: string) {
        this.address = address;
        return this;
    }

    setHobby(hobby: string) {
        this.hobby = hobby;
        return this;
    }

    build() {
        return new Person(this.firstName, this.lastName, this.email, this.phone, this.address, this.hobby);
    }
}