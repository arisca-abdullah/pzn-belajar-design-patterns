import { PersonBuilder } from './PersonBuilder';

const person1 = new PersonBuilder()
    .setFirstName('John')
    .setEmail('john@gmail.com')
    .setHobby("Basketball")
    .build();

const person2 = new PersonBuilder()
    .setFirstName('Jane')
    .setPhone('123')
    .setHobby("Bicycle")
    .build();

const person3 = new PersonBuilder()
    .setFirstName('Jun')
    .setAddress("Guang Dong")
    .setHobby("Kung Fu")
    .build();
