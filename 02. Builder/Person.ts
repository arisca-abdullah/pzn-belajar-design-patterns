export class Person {
    private firstName: string;
    private lastName: string;
    private email: string;
    private phone: string;
    private address: string;
    private hobby: string;

    constructor(firstName: string, lastName: string, email: string, phone: string, address: string, hobby: string) {
        this.firstName = firstName;
        this.lastName = lastName
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.hobby = hobby;
    }

    setFirstName(firstName: string) {
        this.firstName = firstName;
    }

    getFirstName() {
        return this.firstName;
    }

    setLastName(lastName: string) {
        this.lastName = lastName;
    }

    getLastName() {
        return this.lastName;
    }

    setEmail(email: string) {
        this.email = email;
    }

    getEmail() {
        return this.email;
    }

    setPhone(phone: string) {
        this.phone = phone;
    }

    getPhone() {
        return this.phone;
    }

    setAddress(address: string) {
        this.address = address;
    }

    getAddress() {
        return this.address;
    }

    setHobby(hobby: string) {
        this.hobby = hobby;
    }

    getHobby() {
        return this.hobby;
    }
}