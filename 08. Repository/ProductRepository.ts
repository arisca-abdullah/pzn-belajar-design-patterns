import { Connection } from './core/Connection';
import { DatabasePool } from './DatabasePool';
import { Product } from './Product';

export class ProductRepository {
    insert(product: Product) {
        const connection: Connection = DatabasePool.getConnection();
        connection.sql('INSERT INTO products(id, name, price) VALUES (?, ?, ?)', product.getId(), product.getName(), product.getPrice());
        DatabasePool.close(connection);
    }

    update(product: Product) {
        const connection: Connection = DatabasePool.getConnection();
        connection.sql('UPDATE products SET name = ?, price = ? WHERE id = ?', product.getName(), product.getPrice(), product.getId());
        DatabasePool.close(connection);
    }

    delete(id: string) {
        const connection: Connection = DatabasePool.getConnection();
        connection.sql('DELETE products WHERE id = ?', id);
        DatabasePool.close(connection);
    }

    selectAll() {
        const connection: Connection = DatabasePool.getConnection();
        const sqlResult: any[] = connection.select('SELECT * FROM products');
        const products: Product[] = sqlResult.map(result => new Product(result?.id, result?.name, result?.price));
        DatabasePool.close(connection);
        return products;
    }
}