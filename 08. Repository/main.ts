import { Product } from './Product';
import { ProductRepository } from './ProductRepository';

const product: Product = new Product('1', 'Contoh 1', 1000);
const repository: ProductRepository = new ProductRepository();
repository.insert(product);
product.setPrice(2000);
repository.update(product);
repository.delete(product.getId());
const products: Product[] = repository.selectAll();
