import { Connection } from './core/Connection';

export class DatabasePool {
    private static pool: Connection[] = new Array(100).map(i => new Connection('localhost', 'root', '', 'penjualan'));

    static getConnection() {
        if (this.pool.length < 1) {
            throw new Error('No Connection Remaining');
        }

        return this.pool.shift();
    }

    static close(connection: Connection) {
        this.pool.push(connection);
    }
}