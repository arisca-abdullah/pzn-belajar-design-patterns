export class Product {
    private id: string;
    private name: string;
    private price: number;

    constructor(id: string, name: string, price: number) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    setId(id: string) {
        this.id = id;
        return this;
    }

    getId() {
        return this.id;
    }

    setName(name: string) {
        this.name = name;
        return this;
    }

    getName() {
        return this.name;
    }

    setPrice(price: number) {
        this.price = price;
        return this;
    }

    getPrice() {
        return this.price;
    }
}