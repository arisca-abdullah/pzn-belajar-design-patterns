import { EmployeeFactory } from './EmployeeFactory';

const manager1 = EmployeeFactory.createManager('Adam');
const manager2 = EmployeeFactory.createManager('Daniel');
const staff1 = EmployeeFactory.createStaff('Albert');
const staff2 = EmployeeFactory.createStaff('Jane');