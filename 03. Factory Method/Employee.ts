export class Employee {
    private name: string;
    private title: string;
    private salary: number;

    constructor(name: string, title: string, salary: number) {
        this.name = name;
        this.title = title;
        this.salary = salary;
    }

    setName(name: string) {
        this.name = name;
    }

    getName() {
        return this.name;
    }

    setTitle(title: string) {
        this.title = title;
    }

    getTitle() {
        return this.title;
    }

    setSalary(salary: number) {
        this.salary = salary;
    }

    getSalary() {
        return this.salary;
    }
}