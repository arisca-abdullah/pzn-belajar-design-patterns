import { Employee } from './Employee';

export class EmployeeFactory {
    static createManager(name: string) {
        return new Employee(name, 'manager', 10000000);
    }

    static createStaff(name: string) {
        return new Employee(name, 'staff', 5000000);
    }
}