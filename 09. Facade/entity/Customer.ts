import { Address } from './Address';

export class Customer {
    private id: string;
    private name: string;
    private addresses: Address[];

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }

    setId(id: string) {
        this.id = id;
        return this;
    }

    getId() {
        return this.id;
    }

    setName(name: string) {
        this.name = name;
        return this;
    }

    getName() {
        return this.name;
    }

    addAddress(address: Address) {
        this.addresses.push(address);
        return this;
    }

    getAddresses() {
        return this.addresses;
    }

    getAddressById(addressId: string) {
        return this.addresses.find(address => address.getId() === addressId);
    }

    updateAddress(address: Address) {
        const index = this.addresses.findIndex(addr => addr.getId() === address.getId());
        
        if (index >= 0) {
            this.addresses[index] = address;
        };

        return this;
    }

    removeAddress(addressId: string) {
        this.addresses = this.addresses.filter(address => address.getId() != addressId);
        return this;
    }
}