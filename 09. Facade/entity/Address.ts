export class Address {
    private id: string;
    private street: string;
    private country: string;

    constructor(id: string, street: string, country: string) {
        this.id = id;
        this.street = street;
        this.country = country;
    }

    setId(id: string) {
        this.id = id;
        return this;
    }

    getId() {
        return this.id;
    }

    setStreet(street: string) {
        this.street = street;
        return this;
    }

    getStreet() {
        return this.street;
    }

    setCountry(country: string) {
        this.country = country;
        return this;
    }

    getCountry() {
        return this.country;
    }
}