export class Account {
    private no: string;
    private balance: number;

    constructor(no: string, balance: number) {
        this.no = no;
        this.balance = balance;
    }

    setNo(no: string) {
        this.no = no;
        return this;
    }

    getNo() {
        return this.no;
    }

    setBalance(balance: number) {
        this.balance = balance;
        return this;
    }

    getBalance() {
        return this.balance;
    }
}