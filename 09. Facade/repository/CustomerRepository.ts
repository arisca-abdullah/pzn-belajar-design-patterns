import { Connection } from '../core/Connection';
import { DatabasePool } from '../DatabasePool';
import { Customer } from '../entity/Customer';

export class CustomerRepository {
    save(customer: Customer) {
        const connection: Connection = DatabasePool.getConnection();
        connection.sql('INSERT INTO customers (id, name) VALUES (?, ?)', customer.getId(), customer.getName());
        DatabasePool.close(connection);
    }
}