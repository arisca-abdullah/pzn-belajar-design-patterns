import { Connection } from '../core/Connection';
import { DatabasePool } from '../DatabasePool';
import { Account } from '../entity/Account';

export class AccountRepository {
    findById(no: string) {
        const connection: Connection = DatabasePool.getConnection();
        const sqlResult = connection.select('SELECT * FROM accounts WHERE no = ?', no);
        DatabasePool.close(connection);
        return new Account(no, 2000000);
    }

    update(account: Account) {
        const connection: Connection = DatabasePool.getConnection();
        connection.sql('UPDATE accounts SET balance = ? WHERE no = ?', account.getBalance(), account.getNo());
        DatabasePool.close(connection);
    }
}