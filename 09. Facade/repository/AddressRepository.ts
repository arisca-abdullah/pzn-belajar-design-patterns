import { Connection } from '../core/Connection';
import { DatabasePool } from '../DatabasePool';
import { Address } from '../entity/Address';

export class AddressRepository {
    save(address: Address) {
        const connection: Connection = DatabasePool.getConnection();
        connection.sql('INSERT INTO customers (id, street, country) VALUES (?, ?, ?)', address.getId(), address.getStreet(), address.getCountry());
        DatabasePool.close(connection);
    }
}