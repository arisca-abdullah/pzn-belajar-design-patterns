import { Address } from "./entity/Address";
import { Customer } from "./entity/Customer";
import { CustomerFacade } from './facade/CustomerFacade';
import { BankFacade } from './facade/BackFacade';

const customer: Customer = new Customer('1', 'Eko');
const address1: Address = new Address('1', 'Subang', 'Indonesia');
const address2: Address = new Address('2', 'Jakarta', 'Indonesia');

customer.addAddress(address1);
customer.addAddress(address2);

const customerFacade: CustomerFacade = new CustomerFacade();
customerFacade.save(customer);

const customerJSON = customerFacade.toJson(customer);

// ------------------------------------------------------------ //

const bankFacade: BankFacade = new BankFacade();
bankFacade.transfer('0001', '0002', 1000000);
bankFacade.transfer('0001', '0002', 500000);