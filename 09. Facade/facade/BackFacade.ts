import { Account } from "../entity/Account";
import { AccountRepository } from '../repository/AccountRepository';

export class BankFacade {
    private accountRepository: AccountRepository = new AccountRepository();

    transfer(fromAccountNo: string, toAccountNo: string, amount: number) {
        const account1: Account = this.accountRepository.findById(fromAccountNo);
        const account2: Account = this.accountRepository.findById(toAccountNo);

        account1.setBalance(account1.getBalance() - amount);
        account2.setBalance(account1.getBalance() + amount);

        this.accountRepository.update(account1);
        this.accountRepository.update(account2 );
    }
}