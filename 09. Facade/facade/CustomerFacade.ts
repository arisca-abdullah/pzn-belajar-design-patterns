import { CustomerRepository } from "../repository/CustomerRepository";
import { AddressRepository } from "../repository/AddressRepository";
import { Customer } from '../entity/Customer';

export class CustomerFacade {
    private customerRepository: CustomerRepository = new CustomerRepository();
    private addressRepository: AddressRepository = new AddressRepository();

    save(customer: Customer) {
        this.customerRepository.save(customer);

        for (const address of customer.getAddresses()) {
            this.addressRepository.save(address);
        }
    }

    toJson(customer: Customer) {
        const customerNativeObject = {
            id: customer.getId(),
            name: customer.getName(),
            addresses: customer.getAddresses()
        };

        return JSON.stringify(customerNativeObject);
    }
}