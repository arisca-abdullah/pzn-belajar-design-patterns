import { Category } from './Category';

export class CompositeCategory extends Category {
    private categories: Category[] = [];

    constructor(name: string, categories: Category[]) {
        super(name);
        this.categories = categories;
    }

    getCategories(): Category[] {
        return this.categories;
    }
}