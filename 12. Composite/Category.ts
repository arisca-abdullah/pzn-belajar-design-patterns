export class Category {
    private name: string

    constructor(name: string) {
        this.name = name;
    }

    setName(name: string) {
        this.name = name;
        return this;
    }

    getName(): string {
        return this.name;
    }
}