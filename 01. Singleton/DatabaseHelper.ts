import { Connection } from './core/Connection';

export class DatabaseHelper {
    private static connection: Connection;

    static getConnection() {
        if (!this.connection) {
            this.connection = new Connection('localhost', 'root', '', 'penjualan');
        }

        return this.connection;
    }
}