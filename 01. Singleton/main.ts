import { OrderService } from './OrderService';
import { OrderDetailService } from './OrderDetailService';

const orderService: OrderService = new OrderService();
orderService.save('001');

const orderDetailService: OrderDetailService = new OrderDetailService();
orderDetailService.save('001', 'Indomie');
orderDetailService.save('001', 'Sabun');
orderDetailService.save('001', 'Pepsodent');