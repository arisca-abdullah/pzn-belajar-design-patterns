import { Store } from './Store';

const store1 = new Store('Toko X', 'Jakarta', 'Indonesia', 'Gadget');
const store2 = store1.clone().setName('Toko Z');
const store3 = store1.clone().setName('Toko Y').setCity('Bandung');
const store4 = store3.clone().setName('Toko W').setCategory('Fashion');
