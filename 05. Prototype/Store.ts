export class Store {
    private name: string;
    private city: string;
    private country: string;
    private category: string;

    constructor(name: string, city: string, country: string, category: string) {
        this.name = name;
        this.city = city;
        this.country = country;
        this.category = category;
    }

    setName(name: string) {
        this.name = name;
        return this;
    }

    getName() {
        return this.name;
    }

    setCity(city: string) {
        this.city = city;
        return this;
    }

    getCity() {
        return this.city;
    }

    setCountry(country: string) {
        this.country = country;
        return this;
    }

    getCountry() {
        return this.country;
    }

    setCategory(category: string) {
        this.category = category;
        return this;
    }

    getCategory() {
        return this.category;
    }

    clone() {
        return new Store(this.name, this.city, this.country, this.category);
    }
}